### This script merges any number of git repos into one, then pushes the result to a git server

## Name of the merged repo
merged="merged"

## Git service
gitservice="git@bitbucket.org:nilvalls"
gitservice="git://github.com/nilvalls"

## Repos to merge, LOCAL!
repos=("HighMassTau" "HighMassAnalysis" "TriggerStudies")


## Base directory
base=`pwd`


## Attempt to clone the destination repo from the git server
git clone $gitservice/$merged.git $merged
cloneEC="$?"
if [ $cloneEC != 0 ]; then echo "Terminating merging script..."; exit $cloneEC; fi


## Copy each repo to $merged, assumed to be LOCAL
for repo in ${repos[@]}; do

## Remove remote origin
cd $repo
git remote rm origin
git filter-branch --subdirectory-filter * -- --all

## Copy everything to a new dir, then move dir to $merged and commit
list=`ls -1 | xargs`
mkdir $repo
for item in $list; do git mv $item $repo/; done
	git commit -m "Merged $repo to $merged"

	## Add remote branch
	cd ../$merged
	git remote add $repo $base/$repo
	git pull $repo master
	git remote rm $repo

	## Delete empty repo dirs
	cd ..
	rm -rf $repo
done
cd $merged


## Push it to the server
if [ "$gitservice" == "git://github.com/nilvalls" ]; then
	git remote add origin https://github.com/nilvalls/$merged.git
fi
git push -u origin master
